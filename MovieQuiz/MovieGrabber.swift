//
//  MovieGrabber.swift
//  MovieQuiz
//
//  Created by Renan Greca on 5/6/15.
//  Copyright (c) 2015 renangreca. All rights reserved.
//

import Foundation
import SwiftyJSON

func fetchMovies(limit: Int) -> Array<Movie> {
    let apikey = "asprbevazhnusm6fjwqnk24d"
    
    // create the request
    let url = NSURL(string: "http://api.rottentomatoes.com/api/public/v1.0/lists/dvds/new_releases.json?apikey=\(apikey)&page_limit=\(limit)")
    let data = NSData(contentsOfURL: url!)
    
    if data == nil {
        println("Rotten Tomatoes 💩")
        return []
    }
    
    var error: NSErrorPointer
    let json = NSJSONSerialization.JSONObjectWithData(data!,
      options: NSJSONReadingOptions.MutableContainers, error: nil) as! [String:AnyObject]
    
    var returnMovies: Array<Movie> = []
    
    if let movies = json["movies"] as? NSArray {
        for movie in movies {
            let title = movie["title"] as! String
            let synopsis = movie["synopsis"] as! String
            let year = movie["year"] as! Int
            let cleanSynopsis = synopsis.replaceAll(title, with: "_____")
            
            var rating = "unknown"
            if let ratings = movie["ratings"] as? NSDictionary {
                rating = ratings["critics_rating"] as! String
            }
            
            if let posters = movie["posters"] as? NSDictionary {
                let imgURL = posters["original"] as! String
                if let ids = movie["alternate_ids"] as? NSDictionary {
                    let imdbID = ids["imdb"] as! String
                    
                    let m = Movie(title: title, synopsis: cleanSynopsis, imgURL: imgURL, imdbID: imdbID, rating: rating)
                    returnMovies.append(m)
                }
            }
        }
    }
    
    return returnMovies
}

func getURLsAndImages(movies: Array<Movie>) {
    for (var i=0; i<10; ++i) {
        let movie = movies[i]
        getStoreURL(movie)
        getOMDbImg(movie)
        getImageFromURL(movie)
    }
}

func getStoreURL(movie: Movie) {
    if movie._itunesURL == "" {
        let cleanTitle = movie._title.replaceAll(" ", with: "+")
        let url = NSURL(string: "https://itunes.apple.com/search?entity=movie&term=\(cleanTitle)")
        let data = NSData(contentsOfURL: url!)
        
        if data == nil {
            println("iTunes 💩")
            return;
        }
        
        var error: NSErrorPointer
        let json = JSON(data: data!)
        
        movie._itunesURL = json["results"][0]["trackViewUrl"].stringValue
        movie._itimgURL = json["results"][0]["artworkUrl100"].stringValue
    }
}

func getOMDbImg(movie: Movie) {
    if (movie._img == UIImage(named: "MovieQuizLogo")) {
        let url = NSURL(string: "http://www.omdbapi.com/?i=\(movie._imdbID)&plot=short&r=json")
        let data = NSData(contentsOfURL: url!)
        
        if data == nil {
            println("OMDb 💩")
            return
        }
        
        var error: NSErrorPointer
        let json = JSON(data: data!)
        
        movie._imgURL = json["Poster"].stringValue
        
        let imgdata = NSData(contentsOfURL: NSURL(string: movie._imgURL)!)
        if imgdata != nil {
            if let img = UIImage(data: imgdata!) {
                movie._img = img
            }
        }
    }
}

func getImageFromURL(movie: Movie) {
    if (movie._img == UIImage(named: "MovieQuizLogo")) {
        let url = NSURL(string: movie._imgURL)
        let data = NSData(contentsOfURL: url!)
        
        if data == nil {
            println("RT img 💩")
            return
        }
        
        if let img = UIImage(data: data!) {
            movie._img = img
        }
    }
}





